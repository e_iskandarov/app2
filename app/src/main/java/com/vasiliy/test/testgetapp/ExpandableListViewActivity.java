package com.vasiliy.test.testgetapp;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class ExpandableListViewActivity extends AppCompatActivity {

    ExpandableListView mExpandableListView;

    void run(Runnable  r)
    {
        r.run();
    }
    void q()
    {}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expandable_list_view);

        run(() -> {
            Log.d("1", "2");
        });

        mExpandableListView = (ExpandableListView) findViewById(R.id.expLV);

        ArrayList<ArrayList<String>> groups = new ArrayList<ArrayList<String>>();
        ArrayList<String> children1 = new ArrayList<String>();
        ArrayList<String> children2 = new ArrayList<String>();
        ArrayList<String> children3 = new ArrayList<String>();

        children1.add("Text1");
        children1.add("Text2");
        children1.add("Text3");

        children2.add("Text1");
        children2.add("Text2");
        children2.add("Text3");

        children3.add("Text1");
        children3.add("Text2");
        children3.add("Text3");

        groups.add(children1);
        groups.add(children2);
        groups.add(children3);

        ExpListAdapter adapter = new ExpListAdapter(this, groups);
        mExpandableListView.setAdapter(adapter);

        /*
        String firstStr = "kljgy";
        String secondStr = "gser";

        (String firstStr, String secondStr) -> {
            if (firstStr.length() < secondStr.length()) return -1;
            else if (firstStr.length() > secondStr.length()) return 1;
            else return 0;
        };
        */



    }

    public class ExpListAdapter extends BaseExpandableListAdapter {

        private ArrayList<ArrayList<String>> mGroups;
        private Context mContext;

        public ExpListAdapter (Context context, ArrayList<ArrayList<String>> groups){
            mContext = context;
            mGroups = groups;
        }

        @Override
        public int getGroupCount() {
            return mGroups.size();
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return mGroups.get(groupPosition).size();
        }

        @Override
        public Object getGroup(int groupPosition) {
            return mGroups.get(groupPosition);
        }

        @Override
        public Object getChild(int groupPosition, int childPosition) {
            return mGroups.get(groupPosition).get(childPosition);
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView,
                                 ViewGroup parent) {

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.group_view, null);
            }

            if (isExpanded){
                convertView.findViewById(R.id.imgDown).setVisibility(View.INVISIBLE);
                convertView.findViewById(R.id.imgUp).setVisibility(View.VISIBLE);
            }
            else{
                convertView.findViewById(R.id.imgUp).setVisibility(View.INVISIBLE);
                convertView.findViewById(R.id.imgDown).setVisibility(View.VISIBLE);
            }

            TextView textGroup = (TextView) convertView.findViewById(R.id.textGroup);
            textGroup.setText("Group " + Integer.toString(groupPosition));

            return convertView;

        }

        @Override
        public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
                                 View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.child_view, null);
            }

            TextView textChild = (TextView) convertView.findViewById(R.id.textChild);
            textChild.setText(mGroups.get(groupPosition).get(childPosition));

            Button button = (Button)convertView.findViewById(R.id.buttonChild);
            button.setOnClickListener(view -> Toast.makeText(mContext,"button is pressed", Toast.LENGTH_SHORT).show());

            return convertView;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }
    }



}
